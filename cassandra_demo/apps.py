from django.apps import AppConfig


class CassandraDemoConfig(AppConfig):
    name = 'cassandra_demo'
