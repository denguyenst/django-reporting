__author__ = 'Administrator'

from django.conf.urls import url


from . import views

app_name = 'hello'
urlpatterns = [
    #ex: /polls/
    #url(r'^$', views.index, name='index'),
    #ex: /polls/5/
    #url(r'^specifics/(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    #ex: /polls/5/results/
    #url(r'^(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    #ex: /polls/5/vote/
    #url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    #ex: /polls/5/hello
    #url(r'^(?P<parram>[0-9]+)/hello/$', views.hello, name='hello'),

    #Generic View
    url(r'^$', views.hello, name='hello'),
    url(r'^crudops$', views.crudops, name='crudops'),
    url(r'^datamanipulation$', views.datamanipulation, name='datamanipulation'),
    url(r'^article/(?P<articleId>[0-9]+)/$', views.viewArticle),
    url(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.viewArticles, name='articles'),
    url(r'^connection/',views.TemplateView.as_view(), name='template_view'),
    url(r'^login/', views.login, name = 'login'),
    url(r'^search-form/', views.TemplateView1.as_view(), name='search_form'),
    url(r'^search/$', views.search, name='search'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='django.contrib.auth.views.login'),

]