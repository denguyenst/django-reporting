from django.shortcuts import render, redirect
from django.views import generic
from django.http import HttpResponse
from django.core.mail import send_mail
from django.http import HttpResponseRedirect


from hello.models import Dreamreal
from hello.forms import LoginForm, ContactForm
from querybuilder.query import Query

import datetime

def hello(request):
   today = datetime.datetime.now().date()
   daysOfWeek = ['Mon', 'Tue', 'Web', 'Thu', 'Fri', 'Sat', 'Sun']
   return render(request, "hello/index.html", {"today" : today, "days_of_week": daysOfWeek})

def crudops(request):
   dreamreal = Dreamreal(
      website  = "www.chaulaode.com",
      mail     = "chaulaode@ved.com.vn",
      name     = "chau",
      phonenumber = "0975458035"
   )
   dreamreal.save()

   #Read all entries
   objects = Dreamreal.objects.all()
   res = 'Printing all Dreamreal entries in the DB: <br>'

   for elt in objects:
      res += elt.name+"<br>"

   #Read a specific entry:
   chau = Dreamreal.objects.get(name="chau")
   res += "Printing One entry <br>"
   res += chau.name

   #delete a specific entry:
   res += '<br> Deleting an entry <br>'
   chau.delete()

   #update
   dreamreal = Dreamreal(
      website  = "ww.apolo.com",
      mail     = "apolo@ved.com.vn",
      name     = "apolo",
      phonenumber = "01234783472"
   )
   dreamreal.save()
   res += 'Updating entry<br>'

   dreamreal = Dreamreal.objects.get(name = 'apolo')
   dreamreal.name = 'chaulaode'
   dreamreal.save()

   return HttpResponse(res)

def datamanipulation(request):
   res = ''

   #Filtering data:
   qs = Dreamreal.objects.filter(name='paul')
   res += "Found : %s results<br>"%len(qs)

   #Ordering results
   qs = Dreamreal.objects.order_by("name")

   for elt in qs:
      res += elt.name + '<br>'

   return HttpResponse(res)

def viewArticle(request, articleId):
   text = "displaying article Number: %s" %articleId
   #return HttpResponse(text)
   return redirect(viewArticles, year='2045', month='02')

def viewArticles(request, year, month):
   text = "Displaying article of: %s/%s"%(year, month)
   return HttpResponse(text)

def login(request):
   username = "not logged in"

   if request.method == "POST":

      #Get the posted form
      MyLoginForm = LoginForm(request.POST)
      if MyLoginForm.is_valid():
         username = MyLoginForm.cleaned_data['username']
   else:
      MyLoginForm = LoginForm()

   return render(request, 'hello/loggedin.html',{"username": username})

class TemplateView(generic.TemplateView):
    template_name = 'hello/login.html'

class TemplateView1(generic.TemplateView):
    template_name = 'hello/search_form.html'

def search_form(request):
   render(request, 'hello/search_form.html')

def search(request):
   errors = []
   if 'q' in request.GET:
      q = request.GET['q']
      if not q:
         errors.append('Enter a search term.')
      elif len(q)> 20:
          errors.append('Please enter at most 20 characters.')
      else:
          query = Query().from_table('dreamreal')
          query.where(name__contains=q).where(website__contains='any1')
          dreamreals = query.get_sql()
          return HttpResponse(dreamreals)
          #dreamreals = Dreamreal.objects.filter(name__contains=q)
          return render(request, 'hello/search_results.html', {'dreamreals': dreamreals, 'query': q})
   else:
      message = 'You submitted an empty form.'

   return render(request, 'hello/search_form.html', {'errors': errors})

# def contact(request):
#     errors = []
#     if request.method == 'POST':
#         if not request.POST.get('subject', ''):
#             errors.append('Enter a subject.')
#         if not request.POST.get('message', ''):
#             errors.append('Enter a message.')
#         if not request.POST.get('email', '') and '@' not in request.POST['email']:
#             errors.append('Enter a valid e-mail address.')
#         if not errors:
#             send_mail(
#                 request.POST['subject'],
#                 request.POST['message'],
#                 request.POST.get('email', 'noreply@example.com'),
#                 ['siteowner@example.com'],
#             )
#             return HttpResponseRedirect('/contact/thanks/')
#     return render(request, 'hello/contact_form.html',
#         {
#             'errors': errors,
#             'subject': request.POST.get('subject', ''),
#             'message': request.POST.get('message', ''),
#             'email': request.POST.get('email', ''),
#         })

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            send_mail(
                cd['subject'],
                cd['message'],
                cd.get('email', 'noreply@example.com'),
                ['site@example.com'],
            )
            return HttpResponse('Thank you!')
    else:
        form = ContactForm(
            initial={'subject': 'I love your site!'}
        )
    return render(request, 'hello/contact_form.html', {'form': form})