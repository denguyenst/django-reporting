__author__ = 'Administrator'
from django import forms

class SearchFrom(forms.Form):
    from_date = forms.DateField()
    to_date = forms.DateField()