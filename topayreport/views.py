from django.shortcuts import render
from django.http import HttpResponse
from .models import OrderTab
from django.views import generic
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger

from django.views.generic import ListView

from pure_pagination.mixins import PaginationMixin
from django.shortcuts import render_to_response
import time



class IndexView(PaginationMixin, ListView):
    template_name = 'topayreport/index.html'
    model = OrderTab
    paginate_by = 12
    context_object_name = 'order_list'
    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset= OrderTab.objects.order_by('-order_id'))
        return super(ListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)
        context['order_list'] = self.object
        return context

    def get_queryset(self):
        try:
            from_date = int(time.mktime(time.strptime(self.request.GET['from_date'], '%Y-%m-%d')))
            to_date = int(time.mktime(time.strptime(self.request.GET['to_date'], '%Y-%m-%d')))
        except:
            from_date = ''
        if (from_date != ''):
            object_list = self.model.objects.filter(create_time__gte = from_date,create_time__lte = to_date)
        else:
            object_list = self.model.objects.order_by('-order_id')
        return object_list

def index(request):
    paginate_by = 12
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1

    objects = ['john', 'edward', 'josh', 'frank']

    # Provide Paginator with the request object for complete querystring generation

    p = Paginator(objects, request=request, per_page=12)

    people = p.page(page)

    return render(request, 'topayreport/index_test.html', {
        'order_list': people,
    })