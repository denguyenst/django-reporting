# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AirpayDaily(models.Model):
    type = models.IntegerField()
    date = models.DateField()
    location = models.CharField(max_length=2)
    extra = models.CharField(max_length=128)
    data = models.TextField()

    class Meta:
        managed = False
        db_table = 'airpay_daily'
        unique_together = (('type', 'date', 'location', 'extra'),)


class AirpayRealtime(models.Model):
    type = models.IntegerField()
    date = models.DateField()
    tick = models.IntegerField()
    location = models.CharField(max_length=2)
    data = models.TextField()

    class Meta:
        managed = False
        db_table = 'airpay_realtime'
        unique_together = (('type', 'date', 'tick', 'location'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BankAccountTab(models.Model):
    id = models.BigIntegerField(primary_key=True)
    uid = models.BigIntegerField()
    channel_id = models.IntegerField()
    account_no = models.CharField(max_length=64)
    type = models.IntegerField()
    flag = models.IntegerField()
    create_time = models.IntegerField()
    bind_time = models.IntegerField()
    update_time = models.IntegerField()
    mobile_no = models.CharField(max_length=16)
    ic_no = models.CharField(max_length=32)
    ic_verified = models.IntegerField()
    token = models.CharField(max_length=128)
    extra_data = models.TextField()

    class Meta:
        managed = False
        db_table = 'bank_account_tab'


class ChannelTab(models.Model):
    channel_id = models.IntegerField(primary_key=True)
    country = models.CharField(max_length=2)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=256)
    type = models.IntegerField()
    flag = models.IntegerField()
    gateway = models.IntegerField()
    currency = models.CharField(max_length=3)
    discount = models.FloatField()
    icon = models.CharField(max_length=2048)
    priority = models.IntegerField()
    availability = models.IntegerField()
    extra_data = models.CharField(max_length=4096)
    txn_min = models.BigIntegerField()
    txn_max = models.BigIntegerField()
    min_version_ios = models.IntegerField()
    min_version_android = models.IntegerField()
    message = models.CharField(max_length=1024)

    class Meta:
        managed = False
        db_table = 'channel_tab'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Dreamreal(models.Model):
    website = models.CharField(max_length=50)
    mail = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    phonenumber = models.IntegerField()
    online_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'dreamreal'


class Online(models.Model):
    domain = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'online'


class OrderTab(models.Model):
    order_id = models.BigIntegerField(primary_key=True)
    uid = models.BigIntegerField()
    type = models.IntegerField()
    status = models.IntegerField()
    currency = models.CharField(max_length=3)
    currency_amount = models.BigIntegerField()
    create_time = models.IntegerField()
    expired_time = models.IntegerField()
    valid_time = models.IntegerField()
    update_time = models.IntegerField()
    topup_channel_id = models.IntegerField()
    topup_channel_txn_id = models.CharField(max_length=64)
    topup_account_id = models.CharField(max_length=64)
    payment_channel_id = models.IntegerField()
    payment_channel_txn_id = models.CharField(max_length=64)
    payment_account_id = models.CharField(max_length=64)
    item_id = models.CharField(max_length=16)
    item_amount = models.IntegerField()
    item_name = models.CharField(max_length=64)
    item_image = models.CharField(max_length=256)
    ip = models.IntegerField()
    action_country = models.CharField(max_length=2)
    memo = models.CharField(max_length=16)
    topup_txn_id = models.BigIntegerField()
    payment_txn_id = models.BigIntegerField()
    extra_data = models.TextField()
    event_id = models.BigIntegerField()
    topup_payable_amount = models.BigIntegerField()
    payment_payable_amount = models.BigIntegerField()
    topup_cash_amount = models.BigIntegerField()
    payment_cash_amount = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'order_tab'


class PollsAlbum(models.Model):
    name = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars = models.IntegerField()
    artist = models.ForeignKey('PollsMusician', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'polls_album'


class PollsChoice(models.Model):
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField()
    question = models.ForeignKey('PollsQuestion', models.DO_NOTHING)
    created = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'polls_choice'


class PollsFruit(models.Model):
    name = models.CharField(primary_key=True, max_length=100)

    class Meta:
        managed = False
        db_table = 'polls_fruit'


class PollsGroup(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        managed = False
        db_table = 'polls_group'


class PollsMembership(models.Model):
    date_joined = models.DateField()
    invite_reason = models.CharField(max_length=64)
    group = models.ForeignKey(PollsGroup, models.DO_NOTHING)
    person = models.ForeignKey('PollsPerson', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'polls_membership'


class PollsMusician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    instrument = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'polls_musician'


class PollsPerson(models.Model):
    name = models.CharField(max_length=60)
    shirt_size = models.CharField(max_length=1)

    class Meta:
        managed = False
        db_table = 'polls_person'


class PollsQuestion(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'polls_question'


class UserAccountTab(models.Model):
    id = models.BigIntegerField(primary_key=True)
    uid = models.BigIntegerField()
    account_type = models.IntegerField()
    account = models.CharField(max_length=128)
    bind_time = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'user_account_tab'
        unique_together = (('uid', 'account_type'), ('account', 'account_type'),)
