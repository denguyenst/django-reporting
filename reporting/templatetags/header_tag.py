__author__ = 'Administrator'
from django import template
from urlparse import urlparse, urlunparse, parse_qs
from urllib import urlencode

def table_header(context, headers, url):
    u = urlparse(url)
    query = parse_qs(u.query)
    query.pop('ot', None)
    query.pop('o', None)
    url_query = u._replace(query=urlencode(query, True))
    url_query = url_query.query
    return {
        'headers': headers,
        'url_query':url_query
    }

register = template.Library()
register.inclusion_tag('reporting/table_header.html', takes_context=True)(table_header)