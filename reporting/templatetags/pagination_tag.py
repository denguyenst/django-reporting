__author__ = 'Administrator'
from django import template
from django.http import HttpRequest
from urlparse import urlparse, urlunparse, parse_qs
from urllib import urlencode
import reporting.constants

def table_pagination(context, objects, url):
    u = urlparse(url)
    query = parse_qs(u.query)
    query.pop('page', None)
    url_query = u._replace(query=urlencode(query, True))
    url_query = url_query.query

    return {
        'objects': objects,
        'url_query': url_query,
        'per_page': reporting.constants.NUM_PER_PAGE
    }

register = template.Library()
register.inclusion_tag('reporting/pagination.html', takes_context=True)(table_pagination)