__author__ = 'Administrator'
"""ExportingFiles URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from . import views

app_name = 'exportingfiles'
urlpatterns = [
    url(r'^$', views.today_weather, name='today_weather'),
    url(r'^history/$', views.weather_history, name='weather_history'),
    url(r'^details/(?P<weather_id>\w+)', views.details, name='details'),
    url(r'^towns/$', views.all_towns, name='towns'),

]
