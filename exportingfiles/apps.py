from django.apps import AppConfig


class ExportingfilesConfig(AppConfig):
    name = 'exportingfiles'
