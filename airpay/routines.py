import datetime
import collections
import json

from airpay import properties
from airpay import models

def extract_locations(loc_str):
    locations = loc_str.split('|')
    locations = [loc.upper() for loc in locations]
    return locations

def new_user(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    News = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_NEW_USER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            News[loc] = temp_result

    timeline = {}
    for loc, val in News.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('new_user', 0), data.get('new_bank_user', 0),
                                  data.get('new_transaction_user', 0)))

    return timeline

def total_user(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Totals = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_TOTAL_USER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Totals[loc] = temp_result

    timeline = {}
    for loc, val in Totals.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('total_user', 0), data.get('total_bank_user', 0)))

    return timeline

def active_user(start_date, end_date, loc_str, device_type):
    locations = extract_locations(loc_str)

    Actives = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_ACTIVE_USER,
                                                  date__gte=start_date, date__lte=end_date, extra=device_type,
                                                  location=loc).order_by('date')
        if temp_result:
            Actives[loc] = temp_result

    timeline = {}
    for loc, val in Actives.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('active_user_1', 0), data.get('active_user_7', 0), data.get('active_user_30', 0)))

    return timeline


def transaction_user(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Actives = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_TRANSACTION_USER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Actives[loc] = temp_result

    timeline = {}
    for loc, val in Actives.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('txn_user', 0), data.get('txn_user_a7', 0), data.get('txn_user_a14', 0), data.get('txn_user_a30', 0)))

    return timeline


def churn_user(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Churns = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_CHURN_USER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Churns[loc] = temp_result

    timeline = {}
    for loc, val in Churns.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('churn_user_30', 0)))

    return timeline


def transactions(start_date, end_date, loc_str, channel):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_TRANSACTION,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc,
                                                  extra=channel).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('txn_user', 0), data.get('txn_num', 0), data.get('txn_value', 0)))

    return timeline


def transaction_by_payment(start_date, end_date, loc_str, channel):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_TRANSACTION_BY_PAYMENT,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc,
                                                  extra=channel).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('txn_user', 0), data.get('txn_num', 0), data.get('txn_value', 0)))

    return timeline


def shopee_order(start_date, end_date, loc_str, channel):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_SHOPEE_ORDER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc,
                                                  extra=channel).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('txn_user', 0), data.get('txn_num', 0), data.get('txn_value', 0)))

    return timeline


def version(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)
    android_old = properties.android_version_old
    android_new = properties.android_version_new
    ios_old = properties.ios_version_old
    ios_new = properties.ios_version_new
    Versions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_VERSION,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Versions[loc] = temp_result

    timeline = {}
    for loc, val in Versions.items():
        timeline_android = []
        timeline_ios = []
        android = set()
        ios = set()
        for row in val:
            if not row.extra:
                continue
            data = json.loads(row.data)
            for key in data['android'].keys():
                if key not in ('total'):
                    android.add(float(key))
            for key in data['ios'].keys():
                if key not in ('total'):
                    ios.add(float(key))

        android = list(android)
        android.sort()
        ios = list(ios)
        ios.sort()

        android_row = []
        ios_row = []
        android_row.append('Date')
        ios_row.append('Date')
        for version in android:
            android_row.append('%.4f' % version)
        for version in ios:
            ios_row.append('%.4f' % version)
        timeline_android.append(android_row)
        timeline_ios.append(ios_row)
        for row in val:
            data = json.loads(row.data)
            android_row = []
            ios_row = []
            android_row.append(row.date.strftime('%m/%d'))
            ios_row.append(row.date.strftime('%m/%d'))
            for version in android:
                if row.extra != 'new_format':
                    try:
                        version = android_old[android_new['%.4f' % version]]
                        version = float(version)
                        android_row.append(data['android'].get('%.3f' % version, 0))
                    except:
                        android_row.append(0)
                else:
                    android_row.append(data['android'].get('%.4f' % version, 0))
            for version in ios:
                if row.extra != 'new_format':
                    try:
                        version = ios_old[ios_new['%.4f' % version]]
                        version = float(version)
                        ios_row.append(data['ios'].get('%.3f' % version, 0))
                    except:
                        ios_row.append(0)
                else:
                    ios_row.append(data['ios'].get('%.4f' % version, 0))
            timeline_android.append(android_row)
            timeline_ios.append(ios_row)

        timeline[loc] = (timeline_android, timeline_ios)

    return timeline


def version_active(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)
    android_old = properties.android_version_old
    android_new = properties.android_version_new
    ios_old = properties.ios_version_old
    ios_new = properties.ios_version_new
    locations = extract_locations(loc_str)

    Versions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_VERSION_A30,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Versions[loc] = temp_result

    timeline = {}
    for loc, val in Versions.items():
        timeline_android = []
        timeline_ios = []
        android = set()
        ios = set()
        for row in val:
            if not row.extra:
                continue
            data = json.loads(row.data)
            for key in data['android'].keys():
                if key not in ('total'):
                    android.add(float(key))
            for key in data['ios'].keys():
                if key not in ('total'):
                    ios.add(float(key))

        android = list(android)
        android.sort()
        ios = list(ios)
        ios.sort()

        android_row = []
        ios_row = []
        android_row.append('Date')
        ios_row.append('Date')
        for version in android:
            android_row.append('%.4f' % version)
        for version in ios:
            ios_row.append('%.4f' % version)
        timeline_android.append(android_row)
        timeline_ios.append(ios_row)
        for row in val:
            data = json.loads(row.data)
            android_row = []
            ios_row = []
            android_row.append(row.date.strftime('%m/%d'))
            ios_row.append(row.date.strftime('%m/%d'))
            for version in android:
                if row.extra != 'new_format':
                    try:
                        version = android_old[android_new['%.4f' % version]]
                        version = float(version)
                        android_row.append(data['android'].get('%.3f' % version, 0))
                    except:
                        android_row.append(0)
                else:
                    android_row.append(data['android'].get('%.4f' % version, 0))
            for version in ios:
                if row.extra != 'new_format':
                    try:
                        version = ios_old[ios_new['%.4f' % version]]
                        version = float(version)
                        ios_row.append(data['ios'].get('%.3f' % version, 0))
                    except:
                        ios_row.append(0)
                else:
                    ios_row.append(data['ios'].get('%.4f' % version, 0))
            timeline_android.append(android_row)
            timeline_ios.append(ios_row)

        timeline[loc] = (timeline_android, timeline_ios)

    return timeline

def new_card(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    News = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_NEW_USER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            News[loc] = temp_result

    timeline = {}
    for loc, val in News.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('new_credit_card', 0), data.get('new_bank_account', 0), data.get('new_credit_card', 0)+data.get('new_bank_account', 0)))

    return timeline

def total_card(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Totals = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_TOTAL_USER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Totals[loc] = temp_result

    timeline = {}
    for loc, val in Totals.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('total_credit_card', 0), data.get('total_bank_account', 0), data.get('total_credit_card', 0)+data.get('total_bank_account', 0)))

    return timeline


def bank_account(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Accounts = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_BANK_ACCOUNT,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Accounts[loc] = temp_result

    timeline = {}
    for loc, val in Accounts.items():
        timeline[loc] = {'column_titles': [], 'data': []}  # date, bank1, bank2, ...
        all_banks = set()
        for row in val:
            data = json.loads(row.data)
            banks = data.keys()
            all_banks = all_banks | set(banks)

        all_banks = list(all_banks)
        all_banks.sort()

        timeline[loc]['column_titles'].append('Date')
        for bank in all_banks:
            timeline[loc]['column_titles'].append(bank)

        for row in val:
            timeline_row = []
            data = json.loads(row.data)
            timeline_row.append(row.date.strftime('%m/%d'))
            for bank in all_banks:
                timeline_row.append(data.get(bank, 0))
            timeline[loc]['data'].append(timeline_row)

    return timeline


def credit_card(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Cards = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_CREDIT_CARD,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Cards[loc] = temp_result

    timeline = {}
    for loc, val in Cards.items():
        timeline[loc] = {'column_titles': [], 'data': []}  # date, card1, card2, ...
        all_cards = set()
        for row in val:
            data = json.loads(row.data)
            cards = data.keys()
            all_cards = all_cards | set(cards)

        all_cards = list(all_cards)
        all_cards.sort()

        timeline[loc]['column_titles'].append('Date')
        for card in all_cards:
            timeline[loc]['column_titles'].append(card)

        for row in val:
            timeline_row = []
            data = json.loads(row.data)
            timeline_row.append(row.date.strftime('%m/%d'))
            for card in all_cards:
                timeline_row.append(data.get(card, 0))
            timeline[loc]['data'].append(timeline_row)

    return timeline


#  ======================for app tracking=======================
def home_screen(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Totals = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_HOME_SCREEN,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Totals[loc] = temp_result

    timeline = {}
    for loc, val in Totals.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('phone', 0), data.get('game', 0), data.get('movie', 0), data.get('scan_pay', 0),
                                  data.get('cash_trans', 0), data.get('cash_card', 0), data.get('credit_card', 0), data.get('account', 0)))

    return timeline


def home_banner(start_date, end_date, loc_str, channel):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_HOME_BANNER,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc,
                                                  extra=channel).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('value', 0), ))

    return timeline


def purchase_flow(start_date, end_date, loc_str, channel):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_PURCHASE_FLOW,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc,
                                                  extra=channel).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('list', 0), data.get('select_pay', 0), data.get('pay', 0), data.get('pay_otp', 0), data.get('total_order', 0), data.get('succeed_order', 0),))

    return timeline


def gift(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Totals = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_GIFT,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Totals[loc] = temp_result

    timeline = {}
    for loc, val in Totals.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('entry', 0), data.get('card', 0), data.get('100_baht', 0), data.get('ticket', 0),
                                  data.get('shells', 0)))

    return timeline


def general(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Totals = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_GENERAL,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Totals[loc] = temp_result

    timeline = {}
    for loc, val in Totals.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('assistance', 0), data.get('gesture', 0)))

    return timeline


def ewallet(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_EWALLET,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('starting', 0), data.get('credit', 0), data.get('debit', 0), data.get('ending', 0)))
    return timeline


def gift_tracking(start_date, end_date, loc_str):
    locations = extract_locations(loc_str)

    Transactions = {}
    for loc in locations:
        temp_result = models.Daily.objects.filter(type=properties.TYPE_GIFT_TRACKING,
                                                  date__gte=start_date, date__lte=end_date,
                                                  location=loc).order_by('date')
        if temp_result:
            Transactions[loc] = temp_result

    timeline = {}
    for loc, val in Transactions.items():
        timeline[loc] = []
        for row in val:
            data = json.loads(row.data)
            timeline[loc].append((row.date.strftime('%m/%d'), data.get('e1_gift_101', 0), data.get('e1_mtickets', 0), data.get('e1_sf', 0), data.get('e1_shells', 0)))
    return timeline