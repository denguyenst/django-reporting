from django.db import models

# Create your models here.
class Daily(models.Model):
    type = models.IntegerField()
    date = models.DateField()
    location = models.CharField(max_length=2)
    extra = models.CharField(max_length=128)
    data = models.TextField()
    class Meta:
        unique_together = ('type', 'date', 'location', 'extra')