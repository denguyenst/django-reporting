# Create your views here.
import datetime
import unicodecsv

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.views.decorators.cache import never_cache
from django.http import HttpResponse

from airpay import properties
from airpay import routines
from airpay import forms
import csv

@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def new_user(request):
    #CONFIG = config
    form = forms.DateLocationForm(request.POST)

    location = properties.LOCATION_DEFAULT
    timelines = {}

    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.new_user(start_date, end_date,
                                          location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.new_user(start_date, end_date,
                                          location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'new_user'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay New User')]
        header = ('Date', 'New User', 'New Bank User', 'New Transaction User')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/new_user.html', locals(),
                              RequestContext(request))

@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def total_user(request):
    ##CONFIG = config
    form = forms.DateLocationForm(request.POST)

    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.total_user(start_date, end_date, location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.total_user(start_date, end_date, location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'total_user'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Total User')]
        header = ('Date', 'Total User', 'Total Bank User')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/total_user.html', locals(),
                              RequestContext(request))

@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def active_user(request):
    # CONFIG = config
    form = forms.DateLocationTypeForm(request.POST)
    location = properties.LOCATION_DEFAULT
    device = properties.DEVICE_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            device = form.cleaned_data['device']
            timelines = routines.active_user(start_date, end_date,
                                             location, device)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationTypeForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            device = properties.DEVICE_DEFAULT
            timelines = routines.active_user(start_date, end_date,
                                             location, device)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'active_user'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Active User')]
        header = ('Date', 'A1', 'A7', 'A30')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/active_user.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def transaction_user(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.transaction_user(start_date, end_date,
                                                  location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.transaction_user(start_date, end_date,
                                                  location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'transaction_user'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Transaction Active User')]
        header = ('Date', 'A1', 'A7', 'A14', 'A30')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/transaction_user.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def churn_user(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.churn_user(start_date, end_date,
                                            location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.churn_user(start_date, end_date,
                                            location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'churn_user'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Churn User')]
        header = ('Date', 'C30')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/churn_user.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def transaction(request):
    # CONFIG = config
    form = forms.DateLocationExtraForm(request.POST, type=properties.TYPE_TRANSACTION)
    channel = properties.CHANNEL_DEFAULT
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            channel = form.cleaned_data['channel']
            timelines = routines.transactions(start_date, end_date,
                                              location, channel)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        channel = properties.CHANNEL_DEFAULT
        initial = {'start_date':start_date, 'end_date':end_date, 'channel':channel}
        form = forms.DateLocationExtraForm(initial, type=properties.TYPE_TRANSACTION)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.transactions(start_date, end_date,
                                              location, channel)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'transaction'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (channel, 'AirPay Transactions by Topup')]
        header = ('Date', 'Transaction User', '# of Transactions', 'Transaction Volume')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/transaction.html', locals())


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def transaction_by_payment(request):
    #CONFIG = config
    form = forms.DateLocationExtraForm(request.POST, type=properties.TYPE_TRANSACTION_BY_PAYMENT)
    channel = properties.CHANNEL_DEFAULT
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            channel = form.cleaned_data['channel']
            timelines = routines.transaction_by_payment(start_date, end_date,
                                                        location, channel)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        channel = properties.CHANNEL_DEFAULT
        initial = {'start_date':start_date, 'end_date':end_date, 'channel':channel}
        form = forms.DateLocationExtraForm(initial, type=properties.TYPE_TRANSACTION_BY_PAYMENT)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.transaction_by_payment(start_date, end_date,
                                                        location, channel)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'transaction_by_payment'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (channel, 'AirPay Transactions by Payment')]
        header = ('Date', 'Transaction User', '# of Transactions', 'Transaction Volume')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/transaction_by_payment.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def shopee_order(request):
    # CONFIG = config
    form = forms.DateLocationExtraForm(request.POST, type=properties.TYPE_SHOPEE_ORDER)
    channel = properties.CHANNEL_DEFAULT
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            channel = form.cleaned_data['channel']
            timelines = routines.shopee_order(start_date, end_date,
                                                        location, channel)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        channel = properties.CHANNEL_DEFAULT
        initial = {'start_date':start_date, 'end_date':end_date, 'channel':channel}
        form = forms.DateLocationExtraForm(initial, type=properties.TYPE_SHOPEE_ORDER)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.shopee_order(start_date, end_date,
                                                        location, channel)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'shopee_order'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (channel, 'AirPay Shopee Order')]
        header = ('Date', 'Transaction User', '# of Transactions', 'Transaction Volume')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/shopee_order.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def version(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.version(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.version(start_date, end_date,
                                         location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'version'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay version Distribution')]
        print timelines
        header = ''
        csv.write_csv(writer, title, header, timelines[location][0])
        return response
    return render_to_response('airpay/version.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def version_active(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.version_active(start_date, end_date,
                                                location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.version_active(start_date, end_date,
                                                location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'version_active'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay A30 version Distribution')]
        print timelines
        header = ''
        csv.write_csv(writer, title, header, timelines[location][0])
        return response
    return render_to_response('airpay/version_active.html', locals(),
                              RequestContext(request))

@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def new_card(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)

    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.new_card(start_date, end_date,
                                          location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.new_card(start_date, end_date,
                                          location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'new_card'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay New Card')]
        header = ('Date', 'New Credit Card', 'New Bank Account', 'New Bank Card')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/new_card.html', locals(),
                              RequestContext(request))

@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def total_card(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.total_card(start_date, end_date, location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.total_card(start_date, end_date, location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'total_card'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Total Card')]
        header = ('Date', 'Total Credit Card', 'Total Bank Account', 'Total Bank Card')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/total_card.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def bank_account(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.bank_account(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.bank_account(start_date, end_date,
                                         location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'bank_account'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Bank Account Distribution')]
        header = timelines[location]['column_titles']
        csv.write_csv(writer, title, header, timelines[location]['data'])
        return response
    return render_to_response('airpay/bank_account.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def credit_card(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.credit_card(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.credit_card(start_date, end_date,
                                         location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'credit_card'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Credit Card Distribution')]
        header = timelines[location]['column_titles']
        csv.write_csv(writer, title, header, timelines[location]['data'])
        return response
    return render_to_response('airpay/credit_card.html', locals(),
                              RequestContext(request))


# ==================for app tracking===============
@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def home_screen(request):
    # CONFIG = config
    form = forms.DateLocationForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.home_screen(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.home_screen(start_date, end_date,
                                         location)
    return render_to_response('airpay/home_screen.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def home_banner(request):
    #CONFIG = config
    form = forms.DateLocationExtraForm(request.POST, type=properties.TYPE_HOME_BANNER)

    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            channel = form.cleaned_data['channel']
            timelines = routines.home_banner(start_date, end_date,
                                                        location, channel)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        channel = properties.CHANNEL_DEFAULT
        initial = {'start_date':start_date, 'end_date':end_date, 'channel':channel}
        form = forms.DateLocationExtraForm(initial, type=properties.TYPE_HOME_BANNER)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.home_banner(start_date, end_date,
                                                        location, channel)
    return render_to_response('airpay/home_banner.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def purchase_flow(request):
    #CONFIG = config
    form = forms.DateLocationExtraForm(request.POST, type=properties.TYPE_PURCHASE_FLOW)

    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            channel = form.cleaned_data['channel']
            timelines = routines.purchase_flow(start_date, end_date,
                                                        location, channel)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        channel = properties.CHANNEL_DEFAULT
        initial = {'start_date':start_date, 'end_date':end_date, 'channel':channel}
        form = forms.DateLocationExtraForm(initial, type=properties.TYPE_PURCHASE_FLOW)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.purchase_flow(start_date, end_date,
                                                        location, channel)
    return render_to_response('airpay/purchase_flow.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def gift(request):
    
    form = forms.DateLocationForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.gift(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.gift(start_date, end_date,
                                         location)
    return render_to_response('airpay/gift.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def general(request):
    #CONFIG = config
    form = forms.DateLocationForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.general(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.general(start_date, end_date,
                                         location)
    return render_to_response('airpay/general.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def ewallet(request):
    #CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.ewallet(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.ewallet(start_date, end_date,
                                         location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'ewallet'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Ewallet')]
        header = ('Date', 'Starting', 'Credit', 'Debit', 'Ending')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/ewallet.html', locals(),
                              RequestContext(request))


@login_required
@permission_required('access.airpay', raise_exception=True)
@never_cache
def gift_tracking(request):
    #CONFIG = config
    form = forms.DateLocationForm(request.POST)
    location = properties.LOCATION_DEFAULT
    timelines = {}
    if request.method == 'POST':
        if form.is_valid():
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            location = form.cleaned_data['location']
            timelines = routines.gift_tracking(start_date, end_date,
                                         location)
    else:
        today = datetime.datetime.today()
        start_date = today - datetime.timedelta(days=8)
        end_date = today - datetime.timedelta(days=1)
        initial = {'start_date':start_date, 'end_date':end_date}
        form = forms.DateLocationForm(initial)
        if form.is_valid():
            location = properties.LOCATION_DEFAULT
            timelines = routines.gift_tracking(start_date, end_date,
                                         location)
    if 'download' in request.POST:
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % 'gift_tracking'
        writer = unicodecsv.writer(response)
        title = ['%s - %s' % (location, 'AirPay Gift Tracking')]
        header = ('Date', 'e1_gift_101', 'e1csvtickets', 'e1_sf', 'e1_shells')
        csv.write_csv(writer, title, header, timelines[location])
        return response
    return render_to_response('airpay/gift_tracking.html', locals(),
                              RequestContext(request))