from django import forms

from airpay import properties
from airpay import models

def location_checker(location):
    if not location:
        return properties.LOCATION_DEFAULT
    return location

class DateLocationForm(forms.Form):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'class':'date_picker'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'class':'date_picker'}))
    location = forms.CharField(max_length=64, required=False)

    def clean_location(self):
        return location_checker(self.cleaned_data['location'])

class DateLocationExtraForm(forms.Form):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'class':'date_picker'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'class':'date_picker'}))
    location = forms.CharField(max_length=64, required=False)
    channel = forms.TypedChoiceField(choices=(),
                                     widget=forms.Select(),
                                     coerce=str)

    def __init__(self, *args, **kwargs):
        type = kwargs.pop('type')
        super(DateLocationExtraForm, self).__init__(*args, **kwargs)
        channels = models.Daily.objects.filter(type=type).values('extra').distinct().order_by('extra')
        channel_choices = [(c.get('extra'), c.get('extra')) for c in channels]
        self.fields['channel'] = forms.TypedChoiceField(choices=channel_choices,
                                                        widget=forms.Select(),
                                                        coerce=str)

    def clean_location(self):
        return location_checker(self.cleaned_data['location'])


class DateLocationTypeForm(forms.Form):
    start_date = forms.DateField(widget=forms.DateInput(attrs={'class':'date_picker'}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={'class':'date_picker'}))
    location = forms.CharField(max_length=64, required=False)
    device = forms.TypedChoiceField(choices=(),
                                     widget=forms.RadioSelect,
                                     coerce=str)

    def __init__(self, *args, **kwargs):
        super(DateLocationTypeForm, self).__init__(*args, **kwargs)
        devices_choices = [('', 'All OS'), ('IOS', 'IOS'), ('ANDROID', 'Android'), ('UNKNOWN', 'Unknown')]
        self.fields['device'] = forms.TypedChoiceField(choices=devices_choices,
                                                        widget=forms.RadioSelect(),
                                                        coerce=str,
                                                        required=False)

    def clean_location(self):
        return location_checker(self.cleaned_data['location'])