from django.contrib import admin
from .models import Choice, Question

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']
    list_per_page = 1
    fieldsets = [
        (None,                  {'fields': ['question_text']}),
        ('Date information',    {'fields': ['pub_date'], 'classes': ['collapse']})
    ]
    change_form_template = 'topayreport/index.html'
    #admin.site.index_template = 'topayreport/index.html'
    #admin.autodiscover()
    inlines = [ChoiceInline]


admin.site.register(Question, QuestionAdmin)
